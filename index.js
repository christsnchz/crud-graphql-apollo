const { ApolloServer } = require('apollo-server');
const typeDefs = require('./db/schema');
const resolvers = require('./db/resolver');
const jwt = require('jsonwebtoken');
require('dotenv').config({path:'variables.env'});


//servidor 
const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: ({req}) => {    

    const token = req.headers['authorization'] || '';
    if(token){
      try {
        const usuario = jwt.verify(token, process.env.SECRET_WORD);

        return {
          usuario
        }
        
      } catch (error) {
        console.log('Hubo un error');
        console.log(error);
      }
    }
  }
});

//run the server
server.listen().then( ({url})=>{
  console.log( `Server run in URL ${url}` )
} )