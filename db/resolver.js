const Usuarios = require('../MemoryData/Usuarios');
const Productos = require('../MemoryData/Productos');
const Clientes = require('../MemoryData/Clientes');
const Pedidos = require('../MemoryData/Pedido');


const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');
const Pedido = require('../MemoryData/Pedido');

require('dotenv').config({path:'variables.env'});


const crearToken = (usuario, secreta, expiresIn) => {

  const { id, email, nombre, apellido} = usuario;

  return jwt.sign({id, email, nombre, apellido}, secreta, {expiresIn});
}

const resolvers = {
  Query: {

    obtenerUsuario: async (_, {token}) => {
      const usuarioId = await jwt.verify(token, process.env.SECRET_WORD);
      console.log('usuarioId',usuarioId);
      return usuarioId;
    },

    obtenerProductos: () => {
      return Productos;
    },

    obtenerProducto: (_,{id}) => {
    
      const productoExiste = Productos.filter( x => x.id == id);
      if(productoExiste.length === 0){
        throw new Error('producto no encontrado');
      }

      return productoExiste[0];

    },

    obtenerClientes:() => {      
      return Clientes;
    },

    obtenerClientesVendedor: (_ ,{}, ctx ) => {
      const clientes = Clientes.filter( x => x.id == ctx.usuario.id );
      return clientes;      
    },
    
    obtenerCliente: (_,{id},ctx) => {

      const cliente = Clientes.filter(x => x.id === id);
      if(cliente.length === 0){
        throw new Error('Cliente no encontrado');
      }

      if(cliente[0].vendedor.toString() !== ctx.usuario.id){
        throw new Error('no tienes las credenciales ');
      }

    },

    obtenerPedidos: () => {
      return Pedidos;
    },

    obtenerPedidosVendedor: (_, {}, ctx ) => {
      const pedidos = Pedidos.filter( x => x.id == ctx.usuario.id );
      return pedidos;      
    },

    obtenerPedido: (_,{id},ctx) => {

      const pedidoExiste = Pedidos.filter( x => x.id == id);
      if(pedidoExiste.length === 0){
        throw new Error('Pedido no encontrado');
      }

      if(pedidoExiste[0].vendedor !== ctx.usuario.id){
        throw new Error('no tienes las credenciales');
      }

      return pedidoExiste[0];

    },

    obtenerPedidosEstado: ( _, {estado}, ctx ) => { 
      const pedidos = Pedidos.filter( x => x.id == ctx.usuario.id && x.estado === estado );
      return pedidos;      
    },

    mejoresClientes: () => {
      // TODO 
    },

    buscarProducto: (_,{texto}) => {

      const productos = Productos.filter(x => x.nombre === texto );
      return productos;
    }

  },

  Mutation: {

    nuevoUsuario: async (_, {input}, ctx) => {
      const { email, password } = input;      
      //revisar el usuiario
      const existeUsuario = Usuarios.filter( usuario => usuario.email === email );
      
      if(existeUsuario.length > 0){
        throw new Error('El usuario ya esta registrado');
      }
      
      //hashear
       const salt = await bcryptjs.genSaltSync(10);       
       input.password = await bcryptjs.hash(password, salt);

      //save 
      input.id = Usuarios.length;
      Usuarios.push(input);      
      return input;
    },

    autenticarUsuario: async ( _, {input} ) => {
      

      const {email, password} = input;      
      //si el usuario existe
      const existeUsuario = Usuarios.filter( usuario => usuario.email === email );
      if(existeUsuario.length === 0) {
        throw new Error('El usuario no existe');
      }
      
      //revisar el pass
      const passCorrect = await bcryptjs.compare(password , existeUsuario[0].password );
      if(!passCorrect){
        throw new Error('El password es incorrecto');
      }
      
      //crear el token
      return {
        token: crearToken(existeUsuario[0], process.env.SECRET_WORD, '24h')
      }    
    },

    nuevoProducto: ( _, {input} ) => {

      console.log('here',input);

      input.id = Productos.length;
      Productos.push(input);
      
      return input;
    },

    actualizarProducto: (_, {id, input}) => {

      let productoExiste = Productos.filter( x => x.id == id);
      if(productoExiste.length === 0){
        throw new Error('producto no encontrado');
      }
  
      //esto pq es por memoria
      input.id = id
      Productos[id] = input;

      return input;
    },

    eliminarProdcuto: (_,{id}) => {

      let productoExiste = Productos.filter( x => x.id == id);
      if(productoExiste.length === 0){
        throw new Error('producto no encontrado');
      }


      Productos.splice(id,1);

      return 'Producto Eliminado';      
    },

    nuevoCliente: (_, {input}, ctx ) => {

      const { email } = input;      
      //revisar el usuiario
      const existeCliente = Clientes.filter( usuario => usuario.email === email );

      if(existeCliente.length > 0){
        throw new Error('El cliente ya esta registrado');
      }
      
      input.id = Clientes.length;
      input.vendedor = ctx.usuario.id;
      Clientes.push(input);

      return input;
    },

    actualizarCliente: ( _ ,{id, input}, ctx ) => {

      const cliente = Clientes.filter( usuario => usuario.id === id );
      if(cliente.length === 0){
        throw new Error('El cliente no existe');
      }

      if(cliente[0].vendedor.toString() !== ctx.usuario.id){
        throw new Error('no tienes las credenciales ');
      }

      input.id = id
      Clientes[id] = input;
      return input;
    },

    eliminarCliente: (_, {id}, ctx ) => {

      const clienteExiste = Clientes.filter( x => x.id == id);
      if(clienteExiste.length === 0){
        throw new Error('cliente no encontrado');
      }

      if(clienteExiste[0].vendedor.toString() !== ctx.usuario.id){
        throw new Error('no tienes las credenciales ');
      }

      Clientes.splice(id,1);
      return 'Cliente Eliminado';      
    },

    nuevoPedido: async (_, { input }, ctx ) => {
    
      const {cliente} = input;

      const clienteExiste = Clientes.filter( usuario => usuario.id == cliente );
      if(clienteExiste.length === 0){
        throw new Error('El cliente no esta registrado');
      }
      
      if(clienteExiste[0].vendedor !== ctx.usuario.id){
        throw new Error('no tienes las credenciales ');
      }

      for await ( const articulo of input.pedido ) {
        const {id} = articulo;
        const producto = Productos.filter(x => x.id == id);

        if(articulo.cantidad > producto[0].existencia){
          throw new Error(`El articulo ${producto[0].nombre} excede la cantidad disponible`);
        }else{
          producto[0].existencia =  producto[0].existencia - articulo.cantidad;
        }
      }
      input.id = Pedidos.length;
      input.vendedor = ctx.usuario.id;
      input.fecha = new Date();
      Pedidos.push(input);
  
      return input;
    },

    actualizarPedido: (_, {id,input}, ctx ) => {

      const {cliente} = input;

      const pedidoExiste = Pedidos.filter( x => x.id == id);
      if(pedidoExiste.length === 0){
        throw new Error('Pedido no encontrado');
      }

      const clienteExiste = Clientes.filter( usuario => usuario.id == cliente );
      if(clienteExiste.length === 0){
        throw new Error('El cliente no esta registrado');
      }

      if(clienteExiste[0].vendedor !== ctx.usuario.id){
        throw new Error('no tienes las credenciales');
      }

      if(input.pedido){
        for await ( const articulo of input.pedido ) {
          const { id } = articulo;
          const producto = Productos.filter(x => x.id == id);  
          if(articulo.cantidad > producto[0].existencia){
            throw new Error(`El articulo ${producto[0].nombre} excede la cantidad disponible`);
          }else{
            producto[0].existencia =  producto[0].existencia - articulo.cantidad;
          }
        }
      }

      input.id = id;
      input.vendedor = ctx.usuario.id;
      input.fecha = new Date();
      Pedidos[id] = input;
  
      return input;
    },

    eliminarPedido: (_,{id}, ctx) => {

      const pedidoExiste = Pedidos.filter( x => x.id == id);
      if(pedidoExiste.length === 0){
        throw new Error('pedido no encontrado');
      }

      if(pedidoExiste[0].vendedor.toString() !== ctx.usuario.id){
        throw new Error('no tienes las credenciales ');
      }

      Pedidos.splice(id,1);
      return 'Pedido Eliminado';      

    }
  }
}


module.exports = resolvers;